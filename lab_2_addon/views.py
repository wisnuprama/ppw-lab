from django.shortcuts import render, redirect
from django.http import HttpResponse
from lab_1.views import mhs_name, birth_date, mhs_sex

#Create a list of biodata that you wanna show on webpage:
bio_dict = [
        {'subject' : 'Name', 'value' : mhs_name},\
        {'subject' : 'Birth Date', 'value' : birth_date.strftime('%d %B %Y')},\
        {'subject' : 'Sex', 'value' : mhs_sex}
    ]
    
def index(request):
    response = {
        'bio_dict': bio_dict,
    }
    return render(request, 'description_lab2addon.html', response)