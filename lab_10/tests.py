import os
import environ
import requests

from django.test import TestCase
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import *
from .utils import *
from .csui_helper import *
from .custom_auth import *

env = environ.Env(DEBUG=(bool, False))
environ.Env.read_env('praktikum/.env')


# Create your tests here.

class Lab9Test(TestCase):
    def setUp(self):
        self.username = env('SSO_USERNAME')
        self.password = env('SSO_PASSWORD')


    def test_lab9_api_csuihelper(self):

        access_token = get_access_token(self.username, self.password)
        self.assertNotEqual(access_token, None)

        access_token_non_sso = get_access_token('hehe', 'hehe')
        self.assertEqual(access_token_non_sso, None)

        # hehe hehe
        self.assertEqual(get_client_id(), get_client_id())
        self.assertNotEqual(verify_user(access_token), None)
        self.assertNotEqual(get_data_user(access_token, '1606918055'), None)


    def test_lab9_custom_auth(self):
        response = self.client.post(reverse('lab-10:auth_login'),
                                    data={
                                        'username': 'hehe',
                                        'password': 'hehe'
                                    }, follow=True)
        self.assertIn('salah', response.content.decode('utf-8'))

        response = self.client.get(reverse('lab-10:index'))
        self.assertIn('Login', response.content.decode('utf-8'))
        response = self.client.get(reverse('lab-10:dashboard'), follow=True)
        self.assertIn('Login', response.content.decode('utf-8'))

        self.client.post(reverse('lab-10:auth_login'), data={
            'username': self.username,
            'password': self.password,
        })

        response = self.client.get(reverse('lab-10:index'), follow=True)
        self.assertNotIn('Login', response.content.decode('utf8'))
        self.client.get(reverse('lab-9:auth_logout'), follow=True)
