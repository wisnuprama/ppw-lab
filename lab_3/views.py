from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Diary
from datetime import datetime
import pytz

# Create your views here.
def index(request):
    diary_dict = Diary.objects.all().values()
    respones = {'diary_dict' : convert_queryset_into_json(diary_dict)}

    return render(request, 'to_do_list.html', respones)

def add_activity(request):
    '''
    Belum menemukan testcase yang tepat untuk error handling
    karena ada lab 4 dan coverage harus 100 maka dihindari
    try:
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
            Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
            return redirect('/lab-3/')
        except ValueError:
            diary_dict = Diary.objects.all().values()
            respones = {'error': True,'error_message': 'GAGAL INPUT','diary_dict' : convert_queryset_into_json(diary_dict)}
            return render(request, 'to_do_list.html', respones)
    else:
        return HttpResponseRedirect('/lab_3/')
    '''
    if request.method == 'POST':
        date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
        Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
        return redirect('/lab-3/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val