import os
import environ
import requests

from django.test import TestCase
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import *
from .api_enterkomputer import *
from .csui_helper import *
from .custom_auth import *

env = environ.Env(DEBUG=(bool, False))
environ.Env.read_env('praktikum/.env')


# Create your tests here.

class Lab9Test(TestCase):
    def setUp(self):
        self.username = env('SSO_USERNAME')
        self.password = env('SSO_PASSWORD')

    def test_lab9_api_enterkomputer(self):
        self.assertNotEqual(get_drones(), 0)
        self.assertNotEqual(get_soundcards(), 0)
        self.assertNotEqual(get_opticals(), 0)

    def test_lab9_api_csuihelper(self):

        access_token = get_access_token(self.username, self.password)
        self.assertNotEqual(access_token, None)

        access_token_non_sso = get_access_token('hehe', 'hehe')
        self.assertEqual(access_token_non_sso, None)

        # hehe hehe
        self.assertEqual(get_client_id(), get_client_id())
        self.assertNotEqual(verify_user(access_token), None)
        self.assertNotEqual(get_data_user(access_token, '1606918055'), None)

    def test_lab9_custom_auth(self):
        response = self.client.post(reverse('lab-9:auth_login'),
                                    data={
                                        'username': 'hehe',
                                        'password': 'hehe'
                                    }, follow=True)
        self.assertIn('salah', response.content.decode('utf-8'))

        response = self.client.get(reverse('lab-9:index'))
        self.assertIn('Login', response.content.decode('utf-8'))
        response = self.client.get(reverse('lab-9:profile'), follow=True)
        self.assertIn('Login', response.content.decode('utf-8'))

        self.client.post(reverse('lab-9:auth_login'), data={
            'username': self.username,
            'password': self.password,
        })

        response = self.client.get(reverse('lab-9:index'), follow=True)

        self.assertNotIn('Login', response.content.decode('utf8'))

        self.assertEqual(self.client.session.get('drones', None), None)

        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': 1}))
        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': 2}))
        self.assertEqual(len(self.client.session['drones']), 2)

        self.client.get(reverse('lab-9:del_session_item', kwargs={'key': 'drones', 'id': 1}))
        self.assertEqual(len(self.client.session['drones']), 1)

        self.client.get(reverse('lab-9:clear_session_item', kwargs={'key': 'drones'}))
        self.assertEqual(self.client.session.get('drones', None), None)
        self.client.get(reverse('lab-9:auth_logout'), follow=True)

    def test_lab9_cookie(self):

        response = self.client.get('/lab-9/cookie/login/')
        self.assertIn('Login', response.content.decode('utf8'))

        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-9/cookie/profile', follow=True)
        self.assertIn('Login', response.content.decode('utf8'))

        response = self.client.post('/lab-9/cookie/auth_login/', data={
            'password': 'hehe',
            'username': 'hehe',
        }, follow=True)
        self.assertIn('Salah', response.content.decode('utf8'))

        response = self.client.post('/lab-9/cookie/auth_login/', data={
            'password': 'bodo_amat_lol',
            'username': 'jangan_masukin_username_lol',
        }, follow=True)
        self.assertNotIn('Salah', response.content.decode('utf8'))

        response = self.client.get('/lab-9/cookie/login/', follow=True)
        self.assertNotIn('Login', response.content.decode('utf8'))

        self.client.cookies['user_password'] = 'ganti_password_dengan_hehe'

        response = self.client.get('/lab-9/cookie/profile', follow=True)
        self.assertIn('Kamu tidak punya akses', response.content.decode('utf8'))

        self.client.get('/lab-9/cookie/clear', follow=True)
