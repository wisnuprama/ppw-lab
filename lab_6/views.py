from django.shortcuts import render

response = {
    'author':'Wisnu Pramadhitya Ramadhan',
}
# Create your views here.
def index(request):
    template = 'lab_6/lab_6.html'
    return render(request, template, response)
