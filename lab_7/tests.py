from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseNotFound, HttpRequest
from .views import index, get_mahasiswa_page, validate_npm, friend_page
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import json
# Create your tests here.

class Lab7Test(TestCase):

    def setUp(self):
        model = Friend
        self.m = model(friend_name="www", npm="1606918055")
        self.m.save()
        self.cs = CSUIhelper()

    def test_lab7_url(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab7_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_lab7_models_friend(self):
        str_test = "1606918055 - www"
        
        self.assertEqual(Friend.objects.all().count(), 1)
        self.assertEqual(str_test, self.m.__str__())

    def test_lab7_friend_list(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_lab7_get_friend_list(self):
        req = Client().post('/lab-7/get-friend-data/', {})
        self.assertEqual(HttpResponseNotFound, type(req))
        
        req = Client().get('/lab-7/get-friend-data/')
        self.assertEqual(JsonResponse, type(req))
        
    def test_lab7_mahasiswa_page(self):
        ls = get_mahasiswa_page(1)
        self.assertEqual(len(ls), 100)
    
    def test_lab7_get_mahasiswa(self):
        req = Client().get('/lab-7/get-mahasiswa-list/', {'page':1})
        self.assertEqual(JsonResponse, type(req))
    
    def test_lab7_add_friend(self):
        req = Client().get('/lab-7/add-friend/')
        self.assertEqual(JsonResponse, type(req))

        name = 'www'
        req = Client().post('/lab-7/add-friend/', {
            'name':name,
            'npm':'1606890933',
        })

        self.assertEqual(JsonResponse, type(req))

    def test_lab7_delete_friend(self):
        model = Friend
        m = model(friend_name='name', npm='000')
        m.save()
        req = Client().get('/lab-7/delete-friend/', {
            'npm':m.npm
        })

        self.assertEqual(type(req), JsonResponse)
        self.assertNotEqual(model.objects.all().count(), 2)

        m = model(friend_name='name', npm='000')
        m.save()
        req = Client().get('/lab-7/delete-friend/', {
            'id':m.pk
        })

        self.assertNotEqual(model.objects.all().count(), 2)

    def test_lab7_validate_npm(self):
        npm = '1606918055'
        req = Client().post('/lab-7/validate-npm/', {
            'npm':npm
        })

        self.assertEqual(type(req), JsonResponse)

    def test_lab7_friend_page(self):
        found = resolve('/lab-7/1606918055/')
        cs = CSUIhelper()
        cs.instance.get_client_id()
        cs.instance.get_auth_param_dict()
        cs.instance.get_mahasiswa_data('000')
        self.assertEqual(found.func, friend_page)
