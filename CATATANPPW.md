# CATATAN PPW F

## libs
astroid colorama coverage Django gunicorn isort lazy-object-proxy mccabe pytz six wrapt


## 2017/10/12
### menambahkan page di aplikasi
```
1. tambah template html
2. tambahkan url di urls.py untuk page tersebut
3. buat function di views untuk render page
```
- teknologi proses dbms django namanya ORM, yang memmbuat setiap row di dbms menjadi object model
- http respones code
- heroku cli docs: https://devcenter.heroku.com/articles/heroku-cli
- heroku pake postgre
``` 
otomatis: heroku addons
manual: heroku addons:create heroku-postgresql:hobby-dev --app
```
- manage.py
```
run testing: manage.py test
run testing per-app: coverage run manage.py test <app> -v 2
```

## 2017/10/14 LAB
### datetime
```
datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
    dia request date dari post dan menjadikan date yang di post dari string ke object date dengan regex tersebut.
```
### learn regex
```
https://github.com/zeeshanu/learn-regex
```

## 2017/10/19 Kelas
### functional test
```
file: functional_test.py

    from selenium import webdriver
    browser = webdriver.Firefox()
    browser.get('http://localhost:8000')
    assert('To-Do', self.browser.title)
```

## unit testing
```
is software dev process in which the smallest testable parts of an application, called units, are individually and independetly scrutinized for proper operation. Unit testing can ben done manualli but is often automated.

test django
    1. test url ada apa tidak
    2. test apakah ada func yang sesuai function ada apa tidak, cek di views
    3. test function
    4. test apakah ada return respone atau tidak
    5. test isi respone benar apa tidak
```

## lab 8 nya kok ilang :(

## 2017/11/30 lab 9
- untuk call api menggunakan library requests dengan method get/post/dll tergantung dengan design api
- method di requests link, header, data
- untuk akses api csui menggunakan akun csui dan token dan client id 
```
def get_access_token(username, password):
    try:
        url = "https://akun.cs.ui.ac.id/oauth/token/"

        payload = "username=" + username + "&password=" + password + "&grant_type=password"
        headers = {
            'authorization': "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW43RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxuVHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==",
            'cache-control': "no-cache",
            'content-type': "application/x-www-form-urlencoded"
        }
        response = requests.request("POST", url, data=payload, headers=headers)

        return response.json()["access_token"]
    except Exception as e:
        return None
```
- custom auth:
untuk fitur session menggunakan sso sehingga kita tidak menggunakan user django.
untuk logout session di flush.

- link django
menggunakan method built in django -> reverse untuk call link by namespace:name

- penjelasan views ada di views sebagai docstring

- http itu stateless, tapi kita bisa akalin agar kita bisa holding state:
- cookie disimpan di client dan session tersimpan di server
- untuk login kita menggunakan session id sebagai token antara client dan server sehingga kita tidak perlu menyimpan username password di session
- session dan local storage baru ada di html 5
- request.sessions dan request.COOKIES
- cookie di cek di sisi client dan session di cek di server
- secara sekuriti masih ada hole di session id karena session id masih belum dikroscek ulang jika di copy ke browser lain
