from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Belajar di Pacilkom, nongkrong di Pacilkom, makan di Pacilkom, hampir tidur di Pacilkom,\
                        untung tidak lahir di Pacilkom. Bersama teman-teman bermain untuk menjadi mesin.'

def index(request):
    response = {
        'name': mhs_name, 
        'content': landing_page_content,
    }
    return render(request, 'index_lab2.html', response)