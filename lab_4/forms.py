from django import forms
from .models import Message

class Message_Form(forms.Form):
    error_messages = {
        'required': "This field is required.",
        'invalid': 'Isi input dengan email',
        
    }
    attrs = {
        'class': 'form-control'
    }
    
    name = forms.CharField(label='Nama', required=False, 
                        max_length=27, empty_value='Anonymous', 
                        widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(required=False,
                            widget=forms.EmailInput(attrs=attrs))
    message = forms.CharField(widget=forms.Textarea(attrs=attrs), 
                            required=True,
                            error_messages=error_messages)

    class Meta:
        model = Message